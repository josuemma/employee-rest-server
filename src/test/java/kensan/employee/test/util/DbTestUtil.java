package oracle.doceng.test.util;

import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
// example from

public final class DbTestUtil {

    private DbTestUtil() {}

    public static void resetDbSequences(ApplicationContext applicationContext, String... names) throws SQLException {
        DataSource dataSource = applicationContext.getBean(DataSource.class);
        try (Connection dbConnection = dataSource.getConnection()) {
            //Create SQL statements that reset the auto increment columns and invoke
            //the created SQL statements.
            for (String name: names) {
                try (Statement statement1 = dbConnection.createStatement();
                     Statement statement2 = dbConnection.createStatement();
                ) {

                    statement1.execute("select "+name+".CURRVAL from dual");
                    ResultSet rs = statement1.getResultSet();
                    rs.next();
                    long val = rs.getLong(1);
                    if (val == 0){
                        continue; // already reset;
                    }
                    // this only works in H2 in-memory database
                    statement2.execute("alter sequence "+name+" restart with 1");

                }
            }
        }
    }


}
