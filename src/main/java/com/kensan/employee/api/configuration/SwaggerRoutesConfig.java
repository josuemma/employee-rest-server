package com.kensan.employee.api.configuration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Swagger Config UI to be the default page
 *
 * @author josuemma@gmail.com
 */
@Controller
public class SwaggerRoutesConfig 
{
	@RequestMapping("/")
    public String swaggerIndex() {
        return "redirect:/swagger-ui.html";
    }
	
} 