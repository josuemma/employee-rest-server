package com.kensan.employee.api.configuration;

import static springfox.documentation.builders.PathSelectors.regex;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Predicates.or;

/**
 * Swagger Config for the api docuemntation
 *
 * @author josuemma@gmail.com
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
    public Docket perfectoApi() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.apiInfo(swaggerMetaData())
        		.select().apis(RequestHandlerSelectors.basePackage("com.kensan.employee.api.entrypoints.rest.employee"))
                .paths(or(regex("/legal.*"),regex("/v1.*"),regex("/health.*")))
                .build();
    }
	private ApiInfo swaggerMetaData() {
		Contact contact =
				new Contact("Josue Martinez", "https://github.com/josuemma", "josuemma@gmail.com");

		return new ApiInfoBuilder().title("Employee REST APIs")
				.description("Employee APIs Details for user/admin").contact(contact).version("0.0.1").build();
	}

	private ApiKey apiKey() {
		return new ApiKey("Authorization", "Authorization", "header");
	}

	private BasicAuth basicAuth() {
		return new BasicAuth("XAuth");
	}


	private List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return Lists.newArrayList(new SecurityReference("mykey", authorizationScopes));
	}
}

