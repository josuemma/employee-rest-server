package com.kensan.employee.api.dataproviders.repositories;

import com.kensan.employee.api.core.entities.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * employee Rspository interface extenfing the default CrudRepository functionality in the
 * database table.
 *
 * @author josuemm@gmail.com
 */
@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity,Long> {

    @Query("update #{#entityName} e set e.status='INACTIVE' where e.id=?1")
    @Modifying
    void employeeSoftDelete(Long employeeId);
}
