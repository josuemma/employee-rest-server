package com.oracle.doceng.perfecto.entrypoints.rest.healthcheck;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
@RequestMapping("/health")
@Api(value = "healthcheck", description = "Healthcheck API")
public class HealthCheckEndPoint {

    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public String check() {
        return "Health Testing";
    }

}
