package com.kensan.employee.api.entrypoints.rest.employee;

import com.kensan.employee.api.core.usecases.employee.EmployeeBatchCreationException;
import com.kensan.employee.api.core.usecases.employee.EmployeeNotCreatedException;
import com.kensan.employee.api.core.usecases.employee.EmployeeNotDeletedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

/**
 * Global Exception handler controller
 *
 * @author josuemma@gmail.com
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
 
    @ExceptionHandler(value = { NoSuchElementException.class , EmployeeNotDeletedException.class})
    protected ResponseEntity<Object> employeeInformationNotFound(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "Invalid Employee Id";
        return handleExceptionInternal(ex, bodyOfResponse, 
          new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = { EmployeeNotCreatedException.class })
    protected ResponseEntity<Object> employeeNotCreated(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "Invalid Employee Information";
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = { EmployeeBatchCreationException.class })
    protected ResponseEntity<Object> employeeBatchNotCreated(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "Invalid Employee Batch Information";
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
}