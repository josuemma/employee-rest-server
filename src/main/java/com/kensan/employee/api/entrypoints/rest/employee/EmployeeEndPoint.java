package com.kensan.employee.api.entrypoints.rest.employee;


import com.kensan.employee.api.core.dto.EmployeeDto;
import com.kensan.employee.api.core.dto.ResponseDto;
import com.kensan.employee.api.core.usecases.employee.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;
import java.util.List;


/**
 * Employee endpoints
 *
 * @author josuemm@gmail.com
 */

@RestController
@EnableSwagger2
@RequestMapping("/v1//employee")
@Api(value = "Employees ", description = "Employee Endpoint",  authorizations = {@Authorization(value="basicAuth")})
public class EmployeeEndPoint {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CreateEmployeeUseCase createEmployeeUseCase;

    @Autowired
    private UpdateEmployeeUseCase updateEmployeeUseCase;

    @Autowired
    private GetEmployeeUseCase getEmployeeUseCase;

    @Autowired
    private FindEmployeesUseCase findEmployeesUseCase;

    @Autowired
    private DeleteEmployeeUseCase deleteEmployeeUseCase;

    @Autowired
    BatchCreationEmployeeUseCase batchCreationEmployeeUseCase;

    @ApiOperation(value = "Creates an Employee ")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<?> createEmployee(@RequestBody EmployeeDto employeeDto) {

        EmployeeDto dto = this.createEmployeeUseCase.createEmployee(employeeDto);

        return new ResponseEntity<>(employeeDto, HttpStatus.OK);
    }

    @ApiOperation(value = "Updates an Employee information")
    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public ResponseEntity<?> updateEmpoyee(@RequestBody EmployeeDto employeeDto) {

            EmployeeDto dto = this.updateEmployeeUseCase.updateEmployee(employeeDto);

        return new ResponseEntity<>(employeeDto, HttpStatus.OK);
    }

    @ApiOperation(value = "Gets all the Employees ")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> findEmployees() {

        List<EmployeeDto> dtos = this.findEmployeesUseCase.findEmployees();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @ApiOperation(value = "Find an Employee by ID ")
    @RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
    public ResponseEntity<?>  getEmployee(@PathVariable("employeeId") long employeeId) {

        EmployeeDto employeeDto = this.getEmployeeUseCase.getEmployee(employeeId);

        return new ResponseEntity<>(employeeDto, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete an Employee by ID")
    @RequestMapping(value = "/{employeeId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEmployee(@PathVariable("employeeId") long employeeId) {

        this.deleteEmployeeUseCase.deleteArtifactMetadata(employeeId);


        return new ResponseEntity<>(new ResponseDto(200,"Employee Deleted", "", new Date())
                , new HttpHeaders(), HttpStatus.OK);
    }

    @ApiOperation(value = "Creates all the employees send as part of the payload ")
    @RequestMapping(value = "/batch", method = RequestMethod.POST)
    public ResponseEntity<?> createEmployees(@RequestBody List<EmployeeDto> employees) {

        this.batchCreationEmployeeUseCase.createEmployees(employees);

        return new ResponseEntity<>(new ResponseDto(200,"Employees Created", "", new Date())
                , new HttpHeaders(), HttpStatus.OK);
    }

}
