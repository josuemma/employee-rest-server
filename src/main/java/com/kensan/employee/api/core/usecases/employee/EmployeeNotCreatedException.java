package com.kensan.employee.api.core.usecases.employee;


/**
 * Custom exception when there is an error during employee creation
 *
 * @author josuemma@gmail.com
 */

public class EmployeeNotCreatedException extends RuntimeException {

     public EmployeeNotCreatedException(){
        super();
    }

    public EmployeeNotCreatedException(String msg){
        super(msg);

    }

    public EmployeeNotCreatedException(String msg,Throwable cause){
        super(msg,cause);

    }


}
