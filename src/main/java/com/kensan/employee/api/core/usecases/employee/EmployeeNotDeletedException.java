package com.kensan.employee.api.core.usecases.employee;


/**
 * Custom exception when there is an error during employee deletion
 *
 * @author josuemma@gmail.com
 */

public class EmployeeNotDeletedException extends RuntimeException {

     public EmployeeNotDeletedException(){
        super();
    }

    public EmployeeNotDeletedException(String msg){
        super(msg);

    }

    public EmployeeNotDeletedException(String msg, Throwable cause){
        super(msg,cause);

    }


}
