package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;

public interface UpdateEmployeeUseCase {

    public EmployeeDto updateEmployee(EmployeeDto employeeDto);
}
