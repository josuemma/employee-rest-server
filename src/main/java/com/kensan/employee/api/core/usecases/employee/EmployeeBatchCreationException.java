package com.kensan.employee.api.core.usecases.employee;


/**
 * Custom exception when there is an error during employee batch creation
 *
 * @author josuemma@gmail.com
 */

public class EmployeeBatchCreationException extends RuntimeException {

     public EmployeeBatchCreationException(){
        super();
    }

    public EmployeeBatchCreationException(String msg){
        super(msg);

    }

    public EmployeeBatchCreationException(String msg, Throwable cause){
        super(msg,cause);

    }


}
