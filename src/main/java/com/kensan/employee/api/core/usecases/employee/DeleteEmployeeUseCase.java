package com.kensan.employee.api.core.usecases.employee;

public interface DeleteEmployeeUseCase {

    public void deleteArtifactMetadata(long employeeId);
}
