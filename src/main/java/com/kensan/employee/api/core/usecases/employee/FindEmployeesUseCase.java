package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;

import java.util.List;

public interface FindEmployeesUseCase {

    public List<EmployeeDto> findEmployees();
}
