package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;

public interface GetEmployeeUseCase {

    public EmployeeDto getEmployee(Long employeeId);
}
