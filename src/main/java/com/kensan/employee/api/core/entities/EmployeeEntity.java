package com.kensan.employee.api.core.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Employee Entity class is used to persist/query records in the EMPLOYEE
 * database table.
 *
 * @author josuemma@gmail.com
 */
@Entity
@Getter @Setter @NoArgsConstructor
@Table(name = "EMPLOYEE", schema = "PUBLIC")
@Where(clause="status='ACTIVE'")
public class EmployeeEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private  @Id long employeeId;
    private String firstName;
    private String middleInitial;
    private String lastName;
    private Timestamp dateOfBirth;
    private Timestamp dateOfEmployment;
    private String status = "ACTIVE";

}