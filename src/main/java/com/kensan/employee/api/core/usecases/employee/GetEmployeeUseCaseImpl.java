package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;
import com.kensan.employee.api.core.entities.EmployeeEntity;
import com.kensan.employee.api.dataproviders.repositories.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Use Case class is used to get an employee by id
 *
 * @author josuemma@gmail.com
 */
@Service
public class GetEmployeeUseCaseImpl implements GetEmployeeUseCase{

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public EmployeeDto getEmployee(Long employeeId){

        Optional<EmployeeEntity> employeeEntityOp = employeeRepository.findById(employeeId);

        if (!employeeEntityOp.isPresent()) {
            throw new EmployeeNotFoundException("Invalid Employee ID");
        }

        EmployeeDto employeeDto = modelMapper.map(employeeEntityOp.get(), EmployeeDto.class);

        return employeeDto;
    }
}
