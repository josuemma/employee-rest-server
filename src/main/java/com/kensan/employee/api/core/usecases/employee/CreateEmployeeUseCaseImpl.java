package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;
import com.kensan.employee.api.core.entities.EmployeeEntity;
import com.kensan.employee.api.dataproviders.repositories.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Use Case class is used to persist a new employee
 *
 * @author josuemma@gmail.com
 */
@Service
public class CreateEmployeeUseCaseImpl implements CreateEmployeeUseCase{

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ModelMapper modelMapper;

    private final String ACTIVE_STATUS = "ACTIVE";


    @Override
    @Transactional
    public EmployeeDto createEmployee(EmployeeDto employeeDto){

        EmployeeEntity employeeEntity = modelMapper.map(employeeDto, EmployeeEntity.class);
        employeeEntity.setStatus(ACTIVE_STATUS);

        employeeEntity  = employeeRepository.saveAndFlush(employeeEntity);

        if(employeeEntity.getEmployeeId() <=  0){
            throw new EmployeeNotCreatedException("Invlaid Employee Information");
        }
        employeeDto.setEmployeeId(employeeEntity.getEmployeeId());
        employeeRepository.flush();
        return employeeDto;
    }
}
