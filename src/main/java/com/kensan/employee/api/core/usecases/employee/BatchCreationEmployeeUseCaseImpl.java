package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;
import com.kensan.employee.api.core.entities.EmployeeEntity;
import com.kensan.employee.api.dataproviders.repositories.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Use Case class is used to persist a new employee
 *
 * @author josuemma@gmail.com
 */
@Service
public class BatchCreationEmployeeUseCaseImpl implements BatchCreationEmployeeUseCase{

    @Autowired
    private CreateEmployeeUseCase createEmployeeUseCase;
    @Autowired
    private ModelMapper modelMapper;


    @Transactional
    public void createEmployees(List<EmployeeDto> employees){

        if (employees.size()>0) {
            for (EmployeeDto employeeDto: employees) {
                createEmployeeUseCase.createEmployee(employeeDto);
            }
        }else{
            throw new EmployeeBatchCreationException("Invalid Employee Batch Information");
        }
    }
}
