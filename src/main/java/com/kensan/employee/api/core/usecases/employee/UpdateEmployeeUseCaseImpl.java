package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;
import com.kensan.employee.api.core.entities.EmployeeEntity;
import com.kensan.employee.api.dataproviders.repositories.EmployeeRepository;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Use Case class is used to update employee information
 *
 * @author josuemma@gmail.com
 */
@Service
public class UpdateEmployeeUseCaseImpl implements UpdateEmployeeUseCase{


    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ModelMapper modelMapper;


    @Transactional
    public EmployeeDto updateEmployee(EmployeeDto employeeDto){

        Optional<EmployeeEntity> employeeEntityOp = employeeRepository.findById(employeeDto.getEmployeeId());

        if (!employeeEntityOp.isPresent()) {
            throw new EmployeeNotFoundException("Invalid Employee ID");
        }

        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(employeeDto,employeeEntityOp.get());

        employeeRepository.saveAndFlush(employeeEntityOp.get());

        return employeeDto;
    }


}
