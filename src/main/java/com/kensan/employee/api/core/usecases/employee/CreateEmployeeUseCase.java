package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;

public interface CreateEmployeeUseCase {

    public EmployeeDto createEmployee(EmployeeDto employeeDto);
}
