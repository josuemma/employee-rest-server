package com.kensan.employee.api.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * Employee DTO
 *
 * @author josuemma@gmail.com
 */
@Data
public class EmployeeDto {
    private long employeeId;
    private String firstName;
    private String middleInitial;
    private String lastName;
    private Date dateOfBirth;
    private Date dateOfEmployment;
    private String status;
}
