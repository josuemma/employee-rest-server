package com.kensan.employee.api.core.usecases.employee;


/**
 * Custom exception when there is an error during employee search
 *
 * @author josuemma@gmail.com
 */

public class EmployeeNotFoundException extends RuntimeException {

     public EmployeeNotFoundException(){
        super();
    }

    public EmployeeNotFoundException(String msg){
        super(msg);

    }

    public EmployeeNotFoundException(String msg, Throwable cause){
        super(msg,cause);

    }


}
