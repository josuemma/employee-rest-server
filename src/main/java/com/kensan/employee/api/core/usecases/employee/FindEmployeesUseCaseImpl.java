package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.dto.EmployeeDto;
import com.kensan.employee.api.core.entities.EmployeeEntity;
import com.kensan.employee.api.dataproviders.repositories.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Use Case class is used to find all employees
 *
 * @author josuemma@gmail.com
 */
@Service
public class FindEmployeesUseCaseImpl implements FindEmployeesUseCase{

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public List<EmployeeDto> findEmployees(){

        List<EmployeeEntity> metaData = employeeRepository.findAll();
        List<EmployeeDto> dtos = new ArrayList<EmployeeDto>();

        if (metaData.size()>0) {
            java.lang.reflect.Type targetListType = new TypeToken<List<EmployeeDto>>() {}.getType();
            dtos = modelMapper.map(metaData, targetListType);
        }

        return dtos;
    }
}
