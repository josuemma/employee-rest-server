package com.kensan.employee.api.core.usecases.employee;

import com.kensan.employee.api.core.entities.EmployeeEntity;
import com.kensan.employee.api.dataproviders.repositories.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;



/**
 * Use Case class is used to delete an employee by id
 *
 * @author josuemma@gmail.com
 */
@Service
public class DeleteEmployeeUseCaseImpl implements DeleteEmployeeUseCase {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public void deleteArtifactMetadata(long employeeId){

        Optional<EmployeeEntity> employeeEntityOp = employeeRepository.findById(employeeId);
        if (!employeeEntityOp.isPresent()) {
            throw new EmployeeNotFoundException("Invalid Employee ID");
        }

        employeeRepository.employeeSoftDelete(employeeId);
    }



}
