package com.kensan.employee.api.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * Response DTO
 *
 * @author josuemma@gmail.com
 */
@Data @AllArgsConstructor
public class ResponseDto {
    private long status;
    private String message;
    private String error;
    private Date timestamp;
}
