CREATE SCHEMA IF NOT EXISTS KEZAN;

CREATE TABLE EMPLOYEE (
    id   NUMBER(38) NOT NULL,
    firstName              VARCHAR2(150),
    middleInitial          VARCHAR2(150),
    lastName               VARCHAR2(150),
    dateOfBirth               TIMESTAMP WITH TIME ZONE,
    dateOfEmployment          TIMESTAMP WITH TIME ZONE,
    status                 VARCHAR2(10)
);

ALTER TABLE EMPLOYEE ADD CONSTRAINT employee_pk PRIMARY KEY ( id );